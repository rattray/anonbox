from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from google.appengine.ext import db
from django import forms
from app.models import *
from django.template import RequestContext
import os, sys, datetime, copy, logging, settings, smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEImage import MIMEImage
from google.appengine.api import mail

class globvars():
  pages = [
      {'name':'Home', 'url':'../../'}, # add pages here
    ]
  proj_name = "Parents As Partners Image Drop" # change this!
  founders = [
    ]
  proj_description = "An image drop for the MAPP"
  context = {
      'pages': pages,
      'proj_name': proj_name,
      'founders': founders,
      'proj_description': proj_description,
      }

class UploadFileForm(forms.Form):
  photo1 = forms.FileField(required=False)
  photo2 = forms.FileField(required=False)
  photo3 = forms.FileField(required=False)

def send_app_email(receiver, subject, body, attachments=None):
  logging.warning("in send_app_email")
  logging.warning(attachments)
  if attachments:
    logging.warning('there are attachments')
    mail.send_mail(sender="MAPP Webmaster <mapp.webmaster@gmail.com>",
                   to=receiver,
                   subject=subject,
                   body=body,
                   attachments=attachments)
  else:
    mail.send_mail(sender="MAPP Webmaster <mapp.webmaster@gmail.com>",
                   to=receiver,
                   subject=subject,
                   body=body)
  return "sent"

def send_an_email(receiver, subject, body):
  s = smtplib.SMTP('smtp.gmail.com', 587)
  myGmail = 'intouch.registrator@gmail.com'
  myGMPasswd = 'iamarobot'
  s.ehlo()
  s.starttls()
  s.login(myGmail, myGMPasswd)
  msg = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n%s" 
    %(myGmail, receiver, subject, body))
  s.sendmail(myGmail, [receiver], msg)
  s.quit()

def email_a_file(photo):
  logging.warning('in email_file')
  msg = "You have received a file from UploadToMail.appspot.com"
  subject = 'New File from UploadToMail'
  photoname = photo.name
  attachments = [(photoname, photo.read())]
##  msg = MIMEMultipart()
##  msg.attach(MIMEImage(photo.read()))
##  send_an_email('rattray.alex@gmail.com', 'sup, an image', msg)
  send_app_email(('mapp.webmaster@gmail.com','midatlantic_7ndu@sendtodropbox.com'), subject, msg, attachments)
  photo.close()
  return 1

def report_error(request):
  message = 'something failed!</h1><h3> The administrator has been notified, but you may reach him at rattray.alex@gmail.com if you like. \
    You may also send files to midatlantic_7ndu@sendtodropbox.com and mapp.webmaster@gmail.com instead of uploading them here. </h3>'
  send_app_email('rattray.alex@gmail.com','an error on upload to mail! ',str(request))
  return message
  
def index(request):
  if request.method == 'POST':
    logging.warning(request.FILES)
    logging.warning(request.POST)
    uploadform = UploadFileForm(data=request.POST, files=request.FILES)
    success = 0
    try:
      if uploadform.is_valid() and len(request.FILES) > 0:
        for photo in request.FILES:
          logging.warning('in for loop. '+ photo)
          send = email_a_file(request.FILES[photo])
          success += send
        message = str(success)+' files sent'
      else:
        message = report_error(request)
    except:
     message = report_error(request)
  else:
    uploadform = UploadFileForm()
    message = ''
  gv = globvars
  context = {
    'thispage':'Home',
    'uploadform':uploadform,
    'message':message,
      }
  context = dict(context, **gv.context) #combines the 'local' context with the 'global' context
  context_instance = RequestContext(request, context)
  return render_to_response('index.html', context_instance)
